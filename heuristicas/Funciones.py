import math

class ObjectiveFunction():
    def evaluar(self):
        raise NotImplementedError

#Hansen Function
class hansen_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Hansen Function:")
    
    def evaluar(self, x):
        total=0.0
        for i in range(0, 5):
            total+=(i+1)*math.cos(i*x[0]+i+1)
            for j in range(0, 5):
                total+=(j+1)*math.cos((j+2)*x[1]+j+1)
        return total
    
#De Jong's Function
class dejongs_function(ObjectiveFunction):
    def __init__(self):
        print("De Jong's Function:")
        
    def evaluar(self, valores):
        total=0.0
        for v in valores:
            total+=pow(v,2)
        return total
        
#Axis Parallel Function
class axis_parallel_function(ObjectiveFunction):
    def __init__(self):
        print("Axis parallel Function:")
    
    def evaluar(self, valores):
        total=0.0
        i=1
        for v in valores:
            total+=i*pow(v,2)
            i+=1
        return total


#Rotated hyper-ellipsoid Function
class rotated_he_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Rotated Hyper-ellipsoid Function:")
    
    def evaluar(self, x):
        total=0.0
        for i in range(1, self.n+1):
            inner_total=0.0
            for j in range(1, i+1):
                inner_total+=x[j-1]
            total+=pow(inner_total,2)
        return total

#Rosenbrock's valley function
class rosenbrock_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Rosenbrock's valley Function:")
    
    def evaluar(self, x):
        total=0.0
        for i in range(1, self.n):
            total+=pow(100*(x[i]-pow(x[i-1],2)),2)+pow((1-x[i-1]),2)
        return total    
    
#Rastrigin's function
class rastrigin_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Rastrigin's function:")
    
    def evaluar(self, x):
        total=10*self.n
        for i in range(1, self.n+1):
            total+=pow(x[i-1],2)-(10*math.cos(2*math.pi*x[i-1]))
        return total

#Schwefel's function
class schwefel_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Schwefel's function:")
    
    def evaluar(self, x):
        total=0
        for i in range(1, self.n+1):
            total+=x[i-1]*-1*math.sin(math.sqrt(abs(x[i-1])))
        return total
  
#Griewangk's function
class griewangks_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Griewangk's function:")
    
    def evaluar(self, x):
        total=0
        for i in range(1, self.n+1):
            mult_total=1
            for j in range(1, self.n+1):
                mult_total*=math.cos(x[j-1]/math.sqrt(j))+1
            total+=pow(x[i-1],2)-mult_total
        total=total*(1/1400)
        
        return total
   
#Sum of different power function 
class sum_of_different_power_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Sum of different power function:")
    
    def evaluar(self, x):
        total=0
        for i in range(1, self.n+1):
            total+=pow(abs(x[i-1]),i+1)
        return total
    
#Ackley's function
class ackleys_function(ObjectiveFunction):
    a=20
    b=0.2
    c=2*math.pi
    
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Ackley's function:")
    
    def evaluar(self, x):
        first_sum=0
        second_sum=0
        for i in range(1, self.n+1):
            first_sum+=math.pow(x[i-1],2)
            second_sum+=math.cos(self.c*x[i-1])
        
        return -self.a*math.exp(-self.b*math.sqrt(first_sum/self.n))-math.exp(second_sum/self.n)+self.a+math.exp(1)
    
#Langermann's function TODO
class langermanns_function(ObjectiveFunction):
    m=5
    a=[[3,5],[5,2],[2,1],[1,4],[7,9]]
    c=[1,2,5,2,3]
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        
    def evaluar(self, x):
        total=0
        
        for i in range(1, self.m):
            inner_sum=0
            for j in range(1, self.n+1):
                inner_sum+=math.pow(x[j-1]-self.a[i-1][j-1],2)
                
            total+=self.c[i-1]*math.exp(-inner_sum/math.pi)*math.cos(math.pi*inner_sum)

        return total
    
#Michalewic's function
class michalewiczs_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Langermann's function:")
    
    def evaluar(self, x):
        total=0
        for i in range(1, self.n+1):
            total+=math.sin(x[i-1])*math.pow(math.sin(i*math.pow(x[i-1],2)/math.pi),2)

        return -total

#Branin's function
class branins_function(ObjectiveFunction):
    a=1
    b=5.1/(4*math.pow(math.pi,2))
    c=5/math.pi
    d=6
    e=10
    f=1/(8*math.pi)
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Branin's function:")
    
    def evaluar(self, x):
        return self.a*math.pow((x[1]-self.b*math.pow(x[0],2)+self.c*x[0]-self.d),2)+self.e*(1-self.f)*math.cos(x[0])+self.e
        
#Easom's function
class easoms_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Easom's function:")
    
    def evaluar(self, x):               
        return -math.cos(x[0])*math.cos(x[1])*math.exp(-math.pow((x[0]-math.pi),2)-math.pow((x[1]-math.pi),2))

#Goldstein-Price's function
class goldstein_prices_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Goldstein-Price's function:")
    
    def evaluar(self, x):
        return (1+math.pow((x[0]+1),2)*(19-14*x[0]+3*math.pow(x[0],2)-14*x[1]+6*x[0]*x[1]+3*math.pow(x[1],2)))*(30+math.pow((2*x[0]-3*x[1]),2)*(18-32*x[0]+12*math.pow(x[0],2)+48*x[1]-36*x[0]*x[1]+27*math.pow(x[1],2)))
  
#Six-hump camel back function
class six_hump_camel_back_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Six-hump camel back function:")
    
    def evaluar(self, x):
        return (4-2.1*math.pow(x[0],2)+math.pow(x[0],2)/3)*math.pow(x[0],2)+x[0]*x[1]+(-4+4*math.pow(x[1],2))*math.pow(x[1],2)
    
#Drop Wave function
class drop_wave_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Drop Wave function:")
    
    def evaluar(self, x):
        return -(1+math.cos(12*math.sqrt(math.pow(x[0],2)+math.pow(x[1],2))))/((math.pow(x[0],2)+math.pow(x[1],2)*0.5)+2)
 
#Shubert's function
class shuberts_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Shubert's function:")
    
    def evaluar(self, x):
        pass

#Schaffer's function
class schaffers_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Schaffer's function:")
    
    def evaluar(self, x):
        pass
 
#McCormick function
class mccormick_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("McCormick function:")
    
    def evaluar(self, x):
        return math.sin(x[0]+x[1])+math.pow(x[0]-x[1],2)-1.5*x[0]+2.5*x[1]+1
 
#Multimod function
class multimod_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Multimod function:")
    
    def evaluar(self, x):
        pass
 
#Exponential function
class exponential_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Exponential function:")
    
    def evaluar(self, x):
        pass
    
#Moved axis parallel hyper-ellipsoid function
class moved_axis_parallel_he_function(ObjectiveFunction):
    def __init__(self, inf_limit, sup_limit, n):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        print("Moved axis parallel hyper-ellipsoid function:")
    
    def evaluar(self, x):
        pass
    
class PolinomialFunction(ObjectiveFunction):
    poli_function=""
    
    def __init__(self, inf_limit, sup_limit, n, poli_function):
        self.inf_limit = inf_limit
        self.sup_limit = sup_limit
        self.n = n
        
    def evaluar(self, x, i):
        return i*pow(x,2)