from Heuristica import *
from BinDecoder import *
from operator import itemgetter
import random
import math

class EGA(Heuristica):
    n=0
    pc=0
    pm=0
    lon=0
    b2m=0
    poblacion=[]

    def __init__(self):
        super().__init__()

    def inicializar(self):
        for i in range(0,self.n):
            individuo = self.__genIndividuo(self.lon)
            self.poblacion.append(individuo)
        return self.poblacion

    def recombinar(self,poblacion,vars):
        p_mutada=[]
        for j in range(int(self.b2m)):
            pox1=random.randint(0, self.n)
            pox2=random.randint(0, self.lon-vars)
            individuo=poblacion[pox1]
            carcater=individuo[pox2]
            if(carcater=='0'):
                newCar='1'
            else:
                newCar='0'
            pI=individuo[0:pox2]
            pD=individuo[pox2+1:]
            newIndividuo=pI+newCar+pD
            #print("Individuo:{}, pox2:{}".format(individuo,pox2))
            #print("newIndividuo:{}".format(newIndividuo))
            p_mutada.append(newIndividuo)
        return p_mutada

    def decodificar(self,codificacion, entera, decimal, num_variables):
        #print("Codificacion: {}".format(codificacion))
        valores=[]
        longitud_variable = entera + decimal+1
        v_inicio=0
        v_fin=longitud_variable
        for v in range(0,num_variables):
            variable = codificacion[v_inicio:v_fin]
            valores.append(variable)
            v_inicio=v_fin
            v_fin=2*v_fin
            #print("Variable: {}".format(variable))
        #print(valores)
        valores_reales=[]
        for l in valores:
            #print(l)
            num=BinDecoder(l,decimal)
            #print(num.binToValor())
            valores_reales.append(num.getValor())
        #print(valores_reales)        
        return valores_reales

    def evaluar(self,funcion,poblacion,entero,decimal,nvars):
        p_evaluada = []
        for p in poblacion:
            decof=self.decodificar(p,entero,decimal,nvars)
            valor=funcion.evaluar(decof)
            p_evaluada.append([p,valor])
        return p_evaluada
        
    def ordenarPoblacion(self,poblacion):
        p_evaluada=sorted(poblacion,key=itemgetter(1))
        return p_evaluada

    def duplicarPoblacion(self,poblacion):
        double_poblacion=poblacion+poblacion
        return double_poblacion

    def duplicarPopSimple(self,poblacion):
        poblacion_simple=[]
        x=[row[0] for row in poblacion]
        poblacion_simple=x+x
        return poblacion_simple

    def matarPoblacion(self,poblacion):
        for i in range(self.n):
            #print("I:{}, poblacion:{}".format(i,poblacion))
            poblacion.pop()
            #print("I:{}, poblacion:{}".format(i,poblacion))
        return poblacion
    
    def run(self,ciclos, n, pc, pm, entero, decimal, vars, funcion):
        self.n = n
        self.pc = pc
        self.pm = pm
        self.lon = vars*(entero+decimal+1)
        self.b2m = self.lon*self.n*self.pm
        print("b2m:{}".format(self.b2m))
        poblacion=self.inicializar()
        print("p_inicial:{}".format(poblacion))
        p_evaluada = self.evaluar(funcion,poblacion,entero,decimal,vars)
        print("p_evaluada:{}".format(p_evaluada))
        p_ordenada = self.ordenarPoblacion(p_evaluada)
        print("P_Ordenanda:{}".format(p_ordenada))
        #self.duplicarPopSimple(p_ordenada)
        generacion=0
      
        while(generacion<ciclos):
            generacion += 1
            #pdoble=self.duplicarPoblacion(p_ordenada)
            pdoble=self.duplicarPopSimple(p_ordenada)
            
            for i in range(math.ceil(self.n/2)):
                rand=random.randint(0, 1)
                #rand=0.5
                if(rand<pc):
                    locus=random.randint(1, math.ceil((self.lon/2)+1))
                    #print("part:{}".format(locus))
                    A=pdoble[self.n+i]
                    B=pdoble[2*self.n-1-i]                    
                    #print("A:{}, B:{},i:{}".format(A,B,i))
                    AL=A[0:locus]
                    AM=A[locus:self.lon-locus]
                    AR=A[self.lon-locus:self.lon]
                    BL=B[0:locus]
                    BM=B[locus:self.lon-locus]
                    BR=B[self.lon-locus:self.lon]
                    #print("AL:{},AM:{},AR:{}".format(AL,AM,AR))
                    #print("BL:{},BM:{},BR:{}".format(BL,BM,BR))
                    newA=BL+AM+BR
                    newB=AL+BM+AR
                    #print("newA:{},newB:{}".format(newA,newB))
                    pdoble.pop(self.n+i)
                    pdoble.insert(self.n+i,newA)
                    pdoble.pop(2*self.n-1-i)
                    pdoble.insert(2*self.n-1-i,newB)
            print("p_pareada:{}".format(pdoble))
            p_mutada=self.recombinar(pdoble,vars)
            print("p_mutada:{}".format(p_mutada))
            p_evaluada = self.evaluar(funcion,p_mutada,entero,decimal,vars)
            print("p_evaluada:{}".format(p_evaluada))
            p_ordenada = self.ordenarPoblacion(p_evaluada)
            print("p_ordenanda:{}".format(p_ordenada))
            p_ordenada=self.matarPoblacion(p_ordenada)    
            print("muerte P:{}".format(p_ordenada))
        return p_ordenada        

    def __genIndividuo(self,longitud):
        codificacion=''
        for i in range(longitud):
            val = random.randint(0, 1)
            codificacion+=str(val)
        return codificacion
