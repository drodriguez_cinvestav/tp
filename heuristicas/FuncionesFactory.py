from Funciones import *

class FuncionesFactory:

    def getFuncion(self,name):
        if not name:
            return 0
        #elif(name=="hansen_function" or name=="hansen"):
        #    return hansen_function(inf_limit, sup_limit, n)
        elif(name=="dejongs_function" or name=="dejongs"):
            return dejongs_function()
        elif(name=="axis_parallel_function" or name=="axis_parallel"):
            return axis_parallel_function()
        """elif(name=="rotated_he_function" or name=="rotated_he"):
        #    return rotated_he_function(inf_limit, sup_limit, n)
        #elif(name=="rosenbrock_function" or name=="rosenbrock"):
            return rosenbrock_function(inf_limit, sup_limit, n)
        elif(name=="rastrigin_function" or name=="rastrigin"):
            return rastrigin_function(inf_limit, sup_limit, n)
        elif(name=="schwefel_function" or name=="schwefel"):
            return schwefel_function(inf_limit, sup_limit, n)
        elif(name=="griewangks_function" or name=="griewangks"):
            return griewangks_function(inf_limit, sup_limit, n)
        elif(name=="sum_of_different_power_function" or name=="sum_of_different_power"):
            return sum_of_different_power_function(inf_limit, sup_limit, n)
        elif(name=="ackleys_function" or name=="ackleys"):
            return ackleys_function(inf_limit, sup_limit, n)
        elif(name=="langermanns_function" or name=="langermanns"):
            return langermanns_function(inf_limit, sup_limit, n)
        elif(name=="michalewiczs_function" or name=="michalewiczs"):
            return michalewiczs_function(inf_limit, sup_limit, n)
        elif(name=="branins_function" or name=="branins"):
            return branins_function(inf_limit, sup_limit, n)
        elif(name=="easoms_function" or name=="easoms"):
            return easoms_function(inf_limit, sup_limit, n)
        elif(name=="goldstein_prices_function" or name=="goldstein_prices"):
            return goldstein_prices_function(inf_limit, sup_limit, n)
        elif(name=="six_hump_camel_back_function" or name=="six_hump_camel_back"):
            return six_hump_camel_back_function(inf_limit, sup_limit, n)
        elif(name=="drop_wave_function" or name=="drop_wave"):
            return drop_wave_function(inf_limit, sup_limit, n)
        elif(name=="shuberts_function" or name=="shuberts"):
            return shuberts_function(inf_limit, sup_limit, n)
        elif(name=="schaffers_function" or name=="schaffers"):
            return schaffers_function(inf_limit, sup_limit, n)
        elif(name=="mccormick_function" or name=="mccormick"):
            return mccormick_function(inf_limit, sup_limit, n)
        elif(name=="multimod_function" or name=="multimod"):
            return multimod_function(inf_limit, sup_limit, n)
        elif(name=="exponential_function" or name=="exponential"):
            return exponential_function(inf_limit, sup_limit, n)
        elif(name=="moved_axis_parallel_he_function" or name=="moved_axis_parallel_he"):
            return moved_axis_parallel_he_function(inf_limit, sup_limit, n)"""        