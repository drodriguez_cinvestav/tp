from RMHC import *
from EGA import *

class HeuristicaFactory:

    def getHeuristica(self,heuristica):
        if not heuristica:
            return 0
        elif heuristica=="RMHC" :
            rmhc = RMHC()
            return rmhc
        elif heuristica=="EGA":
            ega=EGA()
            return ega
        return 0
            
