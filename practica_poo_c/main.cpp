#include<iostream>
#include<stdlib.h>
#include <math.h>

using namespace std;

class Figura{
    public:
    virtual double area (void) = 0;
    virtual double perimetro (void) = 0;
};

class Visualisable{
    public:
    virtual void draw (void) = 0;
};

class Cuadrado : public Figura, public Visualisable{
    private:
        double lado;
    
    public:
        Cuadrado(double);
        double area();
        double perimetro();
        void draw();
};

Cuadrado::Cuadrado(double _lado){
    lado=_lado;
}

double Cuadrado::area(){
    return lado*lado;
}

double Cuadrado::perimetro(){
    return lado*4;
}

void Cuadrado::draw(){
    cout<<"* * *"<<endl;
    cout<<"* * *"<<endl;
    cout<<"* * *"<<endl;
}

class Circulo : public Figura, public Visualisable{
    private:
        double radio;
    
    public:
        Circulo(double);
        double area();
        double perimetro();
        void draw();
};

Circulo::Circulo(double _rad){
    radio=_rad;
}

double Circulo::area(){
    return M_PI*radio*radio;
}

double Circulo::perimetro(){
    return M_PI*2*radio;
}

void Circulo::draw(){
    cout<<"  *  "<<endl;
    cout<<"* * *"<<endl;
    cout<<"  *  "<<endl;
}

class Triangulo : public Figura, public Visualisable{
    private:
        double base;
        double altura;
    
    public:
        Triangulo(double,double);
        double area();
        double perimetro();
        void draw();
};

Triangulo::Triangulo(double _base,double _altura){
    base=_base;
    altura=_altura;
}

double Triangulo::area(){
    return (base*altura)/2;
}

double Triangulo::perimetro(){
    return 3*base;
}

void Triangulo::draw(){
    cout<<"    * "<<endl;
    cout<<"  * * *"<<endl;
    cout<<"* * * * *"<<endl;
}

int main(){
    cout<<"Generando un Cuadrado"<<endl;
    Cuadrado c1 = Cuadrado(10);
    double area=c1.area();
    double perimetro=c1.perimetro();
    cout<<area<<endl;
    cout<<perimetro<<endl;
    c1.draw();

    cout<<"Generando un Circulo"<<endl;
    Circulo c = Circulo(10);
    area=c.area();
    perimetro=c.perimetro();
    cout<<area<<endl;
    cout<<perimetro<<endl;
    c.draw();

    cout<<"Generando un Triangulo"<<endl;
    Triangulo t = Triangulo(10,5);
    area=t.area();
    perimetro=t.perimetro();
    cout<<area<<endl;
    cout<<perimetro<<endl;
    t.draw();

    return 0;
}