from Figura import *
from Visualizable import *

class Triangulo(Figura,Visualizable):
    base = 0 
    altura = 0
    
    def __init__(self, base, altura):
        super().__init__()
        self.base=base
        self.altura =altura

    def area(self):
        return self.base*self.altura

    def perimetro(self):
        return self.base*3

    def draw(self):
        print("    *    ")
        print("  * * *  ")
        print("* * * * *")