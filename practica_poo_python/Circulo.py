from Figura import *
from Visualizable import *
import math

class Circulo(Figura,Visualizable):
    radio = 0 
    
    def __init__(self, radio):
        super().__init__()
        self.radio = radio

    def area(self):
        r2=pow(self.radio,2)
        return math.pi*r2

    def perimetro(self):
        return math.pi*self.radio*2

    def draw(self):
        print("  *  ")
        print("* * *")
        print("  *  ")