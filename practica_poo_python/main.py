from Cuadrado import *
from Circulo import *
from Triangulo import *

if __name__ == "__main__":
    print("Creado un Cuadrado")
    c=Cuadrado(10)
    print(c.area())
    print(c.perimetro())
    c.draw()
    
    print("*"*50)
    
    print("Creado un Circulo")
    d=Circulo(10)
    print(d.area())
    print(d.perimetro())
    d.draw()

    print("*"*50)

    print("Creado un Triangulo")
    t=Triangulo(10,10)
    print(t.area())
    print(t.perimetro())
    t.draw()