from Figura import *
from Visualizable import *

class Cuadrado(Figura, Visualizable):
    lado = 0 
    
    def __init__(self, lado):
        super().__init__()
        self.lado = lado

    def area(self):
        return self.lado*self.lado

    def perimetro(self):
        return self.lado*4

    def draw(self):
       print("* * *")
       print("* * *")
       print("* * *")