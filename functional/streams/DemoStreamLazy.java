
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class DemoStreamLazy {

   public static void main(String args[])
   {
	   
		Artist johnColtrane = new Artist("John Coltrane", "US");
		Artist johnLennon = new Artist("John Lennon", "UK");
		Artist paulMcCartney = new Artist("Paul McCartney", "UK");
		Artist georgeHarrison = new Artist("George Harrison", "UK");
		Artist ringoStarr = new Artist("Ringo Starr", "UK");
		List<Artist> membersOfTheBeatles = Arrays.asList(johnLennon, 
		paulMcCartney, georgeHarrison, ringoStarr);
		
		membersOfTheBeatles.stream().filter(
			artist->{
				System.out.println(artist.getName());
				return artist.isFrom("UK");
			}
		).count();
		
		
		
	   
   }

}
