
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;

import java.util.ArrayList;
import java.util.Arrays;
public class DemoStream2 {

   public static void main(String args[])
   {
	   
		/*List<String> collected = new ArrayList<>();
		for (String string : Arrays.asList("a", "b", "hello")) {
			String uppercaseString = string.toUpperCase();
			collected.add(uppercaseString);
		}*/
		List<String> collected = Stream.of("a", "b", "hello")
								.map(string -> string.toUpperCase())
								.collect(Collectors.toList());
		
		collected.stream().map(string -> System.out.println(string)
		
		                      );
	   
   }

}
