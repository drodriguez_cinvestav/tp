/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



public class Ulam {
   
public static int ulam(int n){
	int u=1;
	if(n==1)
		return u;
	else
		return (n%2==0?n/2:(3*n+1));	
}

public static String ulamSequence(int n){
	String u="";
	if(n==1)
		return u;
	else{
		return n%2==0?ulamSequence(n/2)+ulam(n)+",":
		              ulamSequence(3*n+1)+ulam(n)+",";
	}
}

  
 public static void main(String args[])
   {
       System.out.println(ulam(3));
	   System.out.println(ulam(10));
	   System.out.println(ulamSequence(3));
   
   } 
}
/*	
public static String ulamSequence(n){
	String u="";
	if(n==1)
		return "1";
	else{
		
	}
}        
    

}*/
