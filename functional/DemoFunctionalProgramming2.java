
import java.util.function.BinaryOperator;

public final class DemoFunctionalProgramming2 {
	
	public static void main(String args[]){
		BinaryOperator<Long> add=(x,y)-> x+y;
		
		System.out.println(add.apply(2L,4L));
			
		
	} 
}

1,6, 11, 16