
# -----------------------------------------------------------------------------
# calc.py
#
# A simple calculator with variables -- all in one file.
# -----------------------------------------------------------------------------
import math

tokens = (
    'NAME','NUMBER','FLOAT',
    'PLUS','MINUS','TIMES','DIVIDE','EQUALS','EXP','SQRT','LN','LOG', 'LOG2','NE','MOD',
    'LPAREN','RPAREN','SIN','COS','TAN','SEC','CSC','COT','SINH','COSH','TANH','SECH','CSCH','COTH'
    )

# Tokens

t_PLUS   = r'\+'
t_MINUS  = r'-'
t_TIMES  = r'\*'
t_DIVIDE = r'/'
t_EXP    = r'\^'
t_SQRT   = r'sqrt'
t_EQUALS = r'='
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_SIN    = r'sin'
t_COS    = r'cos'
t_TAN    = r'tan'
t_SEC    = r'sec'
t_CSC    = r'csc'
t_COT    = r'cot'
t_SINH   = r'sinh'
t_COSH   = r'cosh'
t_TANH   = r'tanh'
t_SECH   = r'sech'
t_CSCH   = r'csch'
t_COTH   = r'coth'
t_LN     = r'ln'
t_LOG    = r'log'
t_LOG2   = r'log2'
t_NE     = r'ne'    
t_MOD    = r'%'  
t_NAME   = r'(?!(sin|cos|tan|sec|csc|cot|sinh|cosh|tanh|sech|csch|coth|sqrt|ln|log|log2|ne))[a-zA-Z_][a-zA-Z0-9]*'

def t_FLOAT(t):
    r'\d+\.\d+'
    t.value=float(t.value)
    return t

def t_NUMBER(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t

# Ignored characters
t_ignore = " \t"

def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")
    
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
    
# Build the lexer
import ply.lex as lex
lexer = lex.lex()

# Parsing rules

precedence = (
    ('left','PLUS','MINUS'),
    ('left','TIMES','DIVIDE','MOD'),
    ('left','EXP','SQRT','NE'),
    ('right','SIN','COS','TAN','SEC','CSC','COT'),
    ('right','SINH','COSH','TANH','SECH','CSCH','COTH'),
    ('right','LN','LOG','LOG2'),
    ('right','UMINUS'),
    )

# dictionary of names
names = { }

def p_statement_assign(t):
    'statement : NAME EQUALS expression'
    names[t[1]] = t[3]

def p_statement_expr(t):
    'statement : expression'
    print(t[1])

def p_expression_binop(t):
    '''expression : expression PLUS expression
                  | expression MINUS expression
                  | expression TIMES expression
                  | expression DIVIDE expression
                  | expression EXP expression
                  | expression MOD expression
                  | SIN expression 
                  | COS expression
                  | TAN expression
                  | SEC expression
                  | CSC expression
                  | COT expression
                  | SINH expression 
                  | COSH expression
                  | TANH expression
                  | SECH expression
                  | CSCH expression
                  | COTH expression
                  | SQRT expression
                  | LN expression
                  | LOG expression
                  | LOG2 expression
                  | NE expression'''

    if t[2] == '+'  : t[0] = t[1] + t[3]
    elif t[2] == '-': t[0] = t[1] - t[3]
    elif t[2] == '*': t[0] = t[1] * t[3]
    elif t[2] == '/': t[0] = t[1] / t[3]
    elif t[2] == '%': t[0] = t[1] % t[3]
    elif t[2] == '^': t[0] = pow(t[1],t[3])
    elif t[1] == 'sin': t[0] = math.sin(math.radians(t[2]))
    elif t[1] == 'cos': t[0] = math.cos(math.radians(t[2]))
    elif t[1] == 'tan': t[0] = math.tan(math.radians(t[2]))
    elif t[1] == 'sec': t[0] = 1/(math.cos(math.radians(t[2])))
    elif t[1] == 'csc': t[0] = 1/(math.sin(math.radians(t[2])))
    elif t[1] == 'cot': t[0] = 1/(math.tan(math.radians(t[2])))
    elif t[1] == 'sinh': t[0] = math.sinh(math.radians(t[2]))
    elif t[1] == 'cosh': t[0] = math.cosh(math.radians(t[2]))
    elif t[1] == 'tanh': t[0] = math.tanh(math.radians(t[2]))
    elif t[1] == 'sech': t[0] = 1/(math.cosh(math.radians(t[2])))
    elif t[1] == 'csch': t[0] = 1/(math.sinh(math.radians(t[2])))
    elif t[1] == 'coth': t[0] = 1/(math.tanh(math.radians(t[2])))
    elif t[1] == 'sqrt': t[0] = math.sqrt(t[2])
    elif t[1] == 'ln': t[0] = math.log(t[2])
    elif t[1] == 'log': t[0] = math.log10(t[2])
    elif t[1] == 'log2': t[0] = math.log2(t[2])
    elif t[1] == 'ne': t[0] = math.exp(t[2])

def p_expression_uminus(t):
    'expression : MINUS expression %prec UMINUS'
    t[0] = -t[2]

def p_expression_group(t):
    'expression : LPAREN expression RPAREN'
    t[0] = t[2]

def p_expression_number(t):
    'expression : FLOAT'
    t[0] = t[1]

def p_expression_float(t):
    'expression : NUMBER'
    t[0] = t[1]

def p_expression_name(t):
    'expression : NAME'
    try:
        t[0] = names[t[1]]
        
    except LookupError:
        #Solicito valores para cada variable no definida
        val = t[1].isdigit()
        if val:
            print("if")
        else:
            print("Undefined name '%s'" % t[1])
            term=input("Defineme: ")
        names[t[1]] = int(term)
        t[0] = int(term)
        #print("Undefined name '%s'" % t[1])
        #t[0] = 0
        

def p_error(t):
    print("Syntax error at '%s'" % t.value)

import ply.yacc as yacc
parser = yacc.yacc()

while True:
    try:
        s = input('calc > ')   # Use raw_input on Python 2
        if s == "quit":
            exit(0)
    except EOFError:
        break
    parser.parse(s)
