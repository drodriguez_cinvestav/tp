# Distribucion Normal Multivariada  
# Integrantes: Raul Silva, David Rodriguez 

import random as r
import numpy as np
import math as m
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sys 

# Funcion de densidad conjunta
def Funcion_DensidadConjunta(medias,x):
    detSigma = np.linalg.det(covarianza) #Determinante de la covarianza
    diferencia = np.subtract(x,medias) #Diferencia entre x y medias   
    transpuesta = np.transpose(diferencia) #Transpuesta de la diferencia   
    inversa = np.linalg.inv(covarianza) #Inversa de la covarianza
    p_punto1 = np.dot(transpuesta,inversa) 
    p_punto2 = np.dot(p_punto1,diferencia)
    p1 = 1/(m.pow(2*m.pi,n/2)*m.pow(detSigma,1/2))
    p2 = m.exp(-0.5*p_punto2)
    fx = p1 * p2 
    return fx #regresa un escalar

#Genera el dataset
def GenerarDataset(k,n,dataset_i):
    contador = 0
        
    while contador < k:
        z = r.uniform(0,1) #Valor a comparar con el valor devuelto fx
        x = []
        for i in range(0,n): #Se generan los vectores x a evaluar
            x.append(r.uniform(0,1))
        valor_fx = Funcion_DensidadConjunta(medias,x)
        if valor_fx >= z: #Se selecionan a los individuos 
            dataset_i.append(x)
            vector_fx.append(valor_fx)
            vector_z.append(z)
            contador += 1
        if valor_fx < z:
            dataset_r.append(x)
            vector_zr.append(z)

#Generar la matriz de covarianza
def Matriz_covarianza(sigmas,rho,n):
    matriz = []
    for i in range(0,n):
        fila = []
        for j in range(0,n):
            if i == j:
                fila.append(sigmas[i]*sigmas[j]*rho[i][j])
            else:
                fila.append(sigmas[i]*sigmas[j]*rho[i][j])
        matriz.append(fila)     
    for i in range(0,n):
        for j in range(0,n):
            matriz[i][j] = matriz[j][i]        
    return matriz

#Generar la matriz de correlaciones
def Matriz_correlacion(n):
    rho = []
    for i in range(0,n):
        fila = []
        for j in range(0,n):
            if i==j:
                fila.append(1)
            else:
                #fila.append(r.uniform(0.5,1))
                #fila.append(r.uniform(-0.8,-1))
                fila.append(0)
        rho.append(fila)
        
    for i in range(0,n):
        for j in range(0,n):
            rho[i][j] = rho[j][i]    
    return rho

    
if __name__ == '__main__':
    n = 2 #Numero de variables
    k = 500 #Numero de individuos
    sigmas = [] #Vector de desviaciones estandar
    medias = [] #Vector de medias
    rho = [] #Matriz de correlaciones
    covarianza = [] #Matriz de covarianza
    dataset_i = [] #Matriz con n columnas y k filas (individuos)
    dataset_r = [] #Matriz con vectores rechazados
    vector_fx = []
    vector_z = []
    vector_zr = []
    

    for i in range(0,n): #Se llena el vector de sigmas
        sigmas.append(0.1)

    for i in range(0,n): #Se llena el vector de medias
        medias.append(0.5)
         
    rho = Matriz_correlacion(n) # Se genera la matriz de correlacion
    print("\nMatriz de correlacion:")
    for i in range(0,n):
        print(rho[i])
   
    covarianza = Matriz_covarianza(sigmas,rho,n) # Se genera la matriz de covarianza
    print("\nMatriz de covarianza:")
    for i in range(0,n):
        print(covarianza[i])

    determinante = np.linalg.det(covarianza)
    if  determinante <= 0:
        print("\nLa determinante es:",determinante,"no es posible determinar el dataset") 
        sys.exit()

    GenerarDataset(k,n,dataset_i)    
    print("\nDataset Generado:")

    for i in range(0,k):
        print(dataset_i[i])
    if n == 2:    
        dataset_individuos = np.array(dataset_i)
        dataset_rechazados = np.array(dataset_r)
        X = dataset_individuos[:,0]
        Y = dataset_individuos[:,1]
        Z = vector_z
        X_r = dataset_rechazados[:,0]
        Y_r = dataset_rechazados[:,1]
        FX = vector_fx
        plt.scatter(X, Y, c='lightseagreen') #Genera la grafica en 2D
        plt.xlabel('Variable x1')
        plt.ylabel('Variable x2')
        figura = plt.figure()
        grafica = plt.axes(projection='3d')
        grafica.scatter3D(X, Y, Z, c='lightseagreen', marker='o') #Genera la grafica en
        grafica.scatter3D(X_r, Y_r, vector_zr, c='orange', marker='.')
        grafica.plot_trisurf(X,Y,FX,alpha=0.3,linewidth=0,antialiased=False)
        grafica.set_xlabel("Variable x1")
        grafica.set_ylabel("Variable x2")
        grafica.set_zlabel("z")
        print("\nMatriz de correlacion:")
        for i in range(0,n):
            print(rho[i])
        print("\nMatriz de covarianza:")
        for i in range(0,n):
            print(covarianza[i])
        print("\n")
        for i in range(0,n):
            print("media","variable x",i," = ",medias[i])
        for i in range(0,n):
            print("media dataset", "variable x",i," = ",sum(dataset_individuos[:,i]/k))
        plt.show()
    
