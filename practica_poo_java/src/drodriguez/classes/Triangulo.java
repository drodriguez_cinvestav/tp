package drodriguez.classes;

import drodriguez.interfaces.Figura;
import drodriguez.interfaces.Visualisable;

public class Triangulo implements Figura, Visualisable {

	public double base;
	public double altura;

	public Triangulo(double base, double altura) {
		this.base = base;
		this.altura = altura;
	}

	@Override
	public void draw() {
		System.out.println("Dibujando un Triangulo");
		System.out.println("    *    ");
		System.out.println("  * * *  ");
		System.out.println("* * * * *");	
	}

	@Override
	public double area() {
		return (base * altura) / 2;
	}

	@Override
	public double perimetro() {
		return base * 3;
	}
}
