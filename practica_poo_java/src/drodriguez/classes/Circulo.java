package drodriguez.classes;


import drodriguez.interfaces.Figura;
import drodriguez.interfaces.Visualisable;

public class Circulo implements Figura,Visualisable{
	public double radio;
	
	public Circulo(double radio){
		this.radio=radio;
	}
	@Override
	public void draw() {
		System.out.println("Dibujando un Circulo");
		System.out.println("    *    ");
		System.out.println("  * * *  ");
		System.out.println("* * * * *");
		System.out.println("  * * *  ");
		System.out.println("    *    ");
	  }
	@Override
	public double area() {
		return Math.PI*radio*radio;
	}
	@Override
	public double perimetro() {
		return Math.PI*2*radio;
	}
}