package drodriguez.classes;


import drodriguez.interfaces.Figura;
import drodriguez.interfaces.Visualisable;

public class Cuadrado implements Figura, Visualisable {
	
	public double lado;

	public Cuadrado(double lado) {
		this.lado = lado;
		
	}

	@Override
	public void draw() {
		System.out.println("Dibujando un Cuadrado");
		System.out.println("* * *");
		System.out.println("* * *");
		System.out.println("* * *");
	  }

	@Override
	public double area() {
		return lado * lado;
	}

	@Override
	public double perimetro() {
		return lado * 4;
	}
}