package drodriguez.main;


import drodriguez.classes.Circulo;
import drodriguez.classes.Cuadrado;
import drodriguez.classes.Triangulo;

public class test {

	public static void main(String[] args) {

		Cuadrado c= new Cuadrado(100);
		System.out.println("El área del cuadrado es: "+c.area());
		System.out.println("El perimetro del cuadrado es: "+c.perimetro());
		c.draw();
			
		System.out.println("********************************");
		
		Circulo d = new Circulo(100);
		System.out.println("El área del circulo es: "+d.area());
		System.out.println("El perimetro del circulo es: "+d.perimetro());
		d.draw();
	
		
		System.out.println("********************************");
		
		Triangulo t = new Triangulo(10,5);
		System.out.println("El área del triangulo es: "+t.area());
		System.out.println("El perimetro del triangulo es: "+t.perimetro());
		t.draw();
		
		
	}

}
