package drodriguez.interfaces;

public interface Figura {
	
	public double area();
	public double perimetro();
}
