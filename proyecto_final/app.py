from flask import Flask, render_template, request, redirect, url_for, flash
from HeuristicaFactory import *
from FuncionesFactory import *
from Lexer import *

app = Flask(__name__)
app.secret_key = 'many random bytes'

@app.route('/hola')
def hola():
 return "hola mundo"


@app.route('/', methods=['POST', 'GET'])
def index():
    return render_template('index.html')

@app.route('/procesar',methods=['POST', 'GET'])
def procesar():
    #if request.method == 'POST':
    heuristica = request.form['heuristica']
    funcion=request.form['funcion']
    iteraciones=int(request.form['iteraciones'])
    entero=int(request.form['entero'])
    decimal=int(request.form['decimal'])
    n_vars=int(request.form['vars'])
   

    hFactory = HeuristicaFactory()
    fFactory = FuncionesFactory()

    if(funcion=="especial"):
        expresion=request.form['polinomio']
        funcion_objetivo=fFactory.getFuncion("polinomio")
        
    else:
        funcion_objetivo=fFactory.getFuncion(funcion)
    
    h = hFactory.getHeuristica(heuristica)

    
    if heuristica == "RMHC":
        #h.run(Iteraciones,Entero,Decimal,num_variables,funcion)
        #vars=2
        if(funcion=="especial"):
            e=h.run(iteraciones,entero,decimal,n_vars,funcion_objetivo,expresion)
        else:
            e=h.run(iteraciones,entero,decimal,n_vars,funcion_objetivo)
        context = {
            'heuristica':heuristica,
            'funcion':funcion,
            'iteraciones':iteraciones,
            'best':e[0],
            'individuo':e[1],
            'valores':e[2]
        }

    elif heuristica == "EGA":
        individuos=int(request.form['individuos'])
        pc=float(request.form['pc'])
        pm=float(request.form['pm'])
        #h.run(Ciclos,Individuos,P_Cruza,P_mutacion,Entero,Decimal,num_variables,funcion)

        if(funcion=="especial"):
           e=h.run(iteraciones,individuos, pc, pm, entero, decimal, n_vars, funcion_objetivo,expresion)
        else:
            e=h.run(iteraciones,individuos, pc, pm, entero, decimal, n_vars, funcion_objetivo)

        
        context = {
            'heuristica':heuristica,
            'funcion':funcion,
            'iteraciones':iteraciones,
            'best':e,
        }
  
    return render_template('resultado.html', **context)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0",port=8080)
