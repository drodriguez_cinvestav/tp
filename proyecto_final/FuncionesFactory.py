from Funciones import *

class FuncionesFactory:

    def getFuncion(self,name):
        if not name:
            return 0
        elif(name=="hansen_function" or name=="hansen"):
            return hansen_function()
        elif(name=="dejongs_function" or name=="dejongs"):
            return dejongs_function()
        elif(name=="axis_parallel_function" or name=="axis_parallel"):
            return axis_parallel_function()
        elif(name=="rosenbrock_function" or name=="rosenbrock"):
            return rosenbrock_function()
        elif(name=="rastrigin_function" or name=="rastrigin"):
            return rastrigin_function()
        elif(name=="schwefel_function" or name=="schwefel"):
            return schwefel_function()
        elif(name=="griewangks_function" or name=="griewangks"):
            return griewangks_function()
        elif(name=="sum_of_different_power_function" or name=="sum_of_different_power"):
            return sum_of_different_power_function()
        elif(name=="ackleys_function" or name=="ackleys"):
            return ackleys_function()
        elif(name=="langermanns_function" or name=="langermanns"):
            return langermanns_function()
        elif(name=="michalewiczs_function" or name=="michalewiczs"):
            return michalewiczs_function()
        elif(name=="branins_function" or name=="branins"):
            return branins_function()
        elif(name=="easoms_function" or name=="easoms"):
            return easoms_function()
        elif(name=="goldstein_prices_function" or name=="goldstein_prices"):
            return goldstein_prices_function()
        elif(name=="six_hump_camel_back_function" or name=="six_hump_camel_back"):
            return six_hump_camel_back_function()
        elif(name=="drop_wave_function" or name=="drop_wave"):
            return drop_wave_function()
        elif(name=="mccormick_function" or name=="mccormick"):
            return mccormick_function()
        elif(name=="rotated_he_function" or name=="rotated_he"):
            return rotated_he_function()
        elif(name=="polinomio_function" or name=="polinomio"):
            return polinomio_funcion()    