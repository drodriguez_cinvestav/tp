import math
import cmath
import ply.yacc as yacc
import ply.lex as lex

class Lexer():
    result=0
    parser = yacc
    def __init__(self):
        tokens = (
            'NAME','NUMBER',
            'EXP','RAIZ','ABS','POT','PLUS','MINUS','TIMES','DIVIDE','EQUALS',
            'LPAREN','RPAREN',
            'SIN','COS','TAN','COT','SEC','CSC',
            'SINH','COSH','TANH','COTH','SECH','CSCH'
        )


        # Tokens

        t_PLUS    = r'\+'
        t_MINUS   = r'-'
        t_TIMES   = r'\*'
        t_DIVIDE  = r'/'
        t_EQUALS  = r'='
        t_LPAREN  = r'\('
        t_RPAREN  = r'\)'
        t_NAME    = r'(?!(sin|cos|tan|cot|sec|csc|sinh|cosh|tanh|coth|sech|csch|abs|sqrt|exp)) [a-zA-Z_][a-zA-Z0-9_]*'
        t_SIN = r'sin'
        t_COS = r'cos'
        t_TAN = r'tan'
        t_COT = r'cot'
        t_SEC = r'sec'
        t_CSC = r'csc'
        t_SINH = r'sinh'
        t_COSH = r'cosh'
        t_TANH = r'tanh'
        t_COTH = r'coth'
        t_SECH = r'sech'
        t_CSCH = r'csch'
        t_ABS = r'abs'
        t_POT = r'\^'
        t_RAIZ = r'sqrt'
        t_EXP = r'exp'
        def t_NUMBER(t):
            r'[0-9]+(\.[0-9]+)?'
            try:
                t.value = float(t.value)
            except ValueError:
                print("Integer value too large %d", t.value)
                t.value = 0
            return t

        # Ignored characters
        t_ignore = " \t"

        def t_newline(t):
            r'\n+'
            t.lexer.lineno += t.value.count("\n")

        def t_error(t):
            print("Illegal character '%s'" % t.value[0])
            t.lexer.skip(1)


        lexer = lex.lex()

        # Parsing rules

        precedence = (
            ('left','PLUS','MINUS'),
            ('left','TIMES','DIVIDE'),
            ('right','SIN','COS','TAN','COT','SEC','CSC'),
            ('right','SINH','COSH','TANH','COTH','SECH','CSCH'),
            ('right','POT'),
            ('right','ABS'),
            ('right','RAIZ'),
            ('right','EXP'),
            ('right','UMINUS'),
        )

        # dictionary of names
        names = { }

        def p_statement_assign(t):
            'statement : NAME EQUALS expression'
            names[t[1]] = t[3]

        def p_statement_expr(t):
            'statement : expression'
            self.result=t[1]

        def p_expression_binop(t):
            '''expression : expression PLUS expression
                          | expression MINUS expression
                          | expression TIMES expression
                          | expression DIVIDE expression
                          | expression POT expression'''
            if t[2] == '+'  : t[0] = t[1] + t[3]
            elif t[2] == '-': t[0] = t[1] - t[3]
            elif t[2] == '*': t[0] = t[1] * t[3]
            elif t[2] == '/': t[0] = t[1] / t[3]
            else:
                t[0] = pow(t[1],t[3])


        def p_expression_trigonometric(t):
            '''expression : SIN expression
                          | COS expression
                          | TAN expression
                          | COT expression
                          | SEC expression
                          | CSC expression
                          | ABS expression
                          | RAIZ expression
                          | EXP expression'''
            if t[1] == 'sin'  : t[0] = math.sin(math.radians(t[2]))
            elif t[1] == 'cos': t[0] = math.cos(math.radians(t[2]))
            elif t[1] == 'tan': t[0] = math.tan(math.radians(t[2]))
            elif t[1] == 'cot': t[0] = 1/math.tan(math.radians(t[2]))
            elif t[1] == 'sec': t[0] = 1/math.cos(math.radians(t[2]))
            elif t[1] == 'csc': t[0] = 1/math.sin(math.radians(t[2]))
            elif t[1] == 'abs': t[0] = abs(t[2])
            elif t[1] == 'sqrt': t[0] = math.sqrt(t[2])
            elif t[1] == 'exp': t[0] = math.exp(t[2])

        def p_expression_trigonometric_hyperbolic(t):
            '''expression : SINH expression
                          | COSH expression
                          | TANH expression
                          | COTH expression
                          | SECH expression
                          | CSCH expression'''
            if t[1] == 'sinh'  : t[0] = math.sinh(t[2])
            elif t[1] == 'cosh': t[0] = math.cosh(t[2])
            elif t[1] == 'tanh': t[0] = math.tanh(t[2])
            elif t[1] == 'coth': t[0] = 1/math.tanh(t[2])
            elif t[1] == 'sech': t[0] = 1/math.cosh(t[2])
            elif t[1] == 'csch': t[0] = 1/math.sinh(t[2])

        def p_expression_uminus(t):
            'expression : MINUS expression %prec UMINUS'
            t[0] = -t[2]

        def p_expression_group(t):
            'expression : LPAREN expression RPAREN'
            t[0] = t[2]

        def p_expression_number(t):
            'expression : NUMBER'
            t[0] = t[1]

        def p_expression_name(t):
            'expression : NAME'
            try:
                t[0] = names[t[1]]
            except LookupError:
                print("Undefined name '%s'" % t[1])
                t[0] = 0

        def p_syntax(t):
            '''expression : expression PLUS
                          | expression MINUS
                          | expression TIMES
                          | expression DIVIDE '''
            print("Syntax error at '%s'" % t[2])

        def p_error(t):
            print("Syntax error at '%s'" % t.value)

        self.parser = yacc.yacc()

    def execute(self, expression):
        self.parser.parse(expression)
        return self.result
