from Heuristica import *
from BinDecoder import *
from operator import itemgetter
import random
import math

class EGA(Heuristica):
    n=0
    pc=0
    pm=0
    lon=0
    b2m=0
    poblacion=[]
    expresion=""
    def __init__(self):
        super().__init__()

    def inicializar(self):
        self.poblacion=[]
        for i in range(0,self.n):
            individuo = self.__genIndividuo(self.lon)
            self.poblacion.append(individuo)
        return self.poblacion

    def recombinar(self,poblacion,vars):
        p_mutada=poblacion[:]
        for j in range(int(self.b2m)):
            pox1=random.randint(0, self.n)
            pox2=random.randint(0, self.lon-vars)
            #print("nIndivido:{},nCaracter:{}".format(pox1,pox2))
            individuo=poblacion[pox1]
            carcater=individuo[pox2]
            if(carcater=='0'):
                newCar='1'
            else:
                newCar='0'
            pI=individuo[0:pox2]
            pD=individuo[pox2+1:]
            newIndividuo=pI+newCar+pD
            #print("Individuo:{}, pox2:{}".format(individuo,pox2))
            #print("newIndividuo:{},len{}".format(newIndividuo,j))
            p_mutada.pop(pox1)
            p_mutada.insert(pox1,newIndividuo)
        return p_mutada

    def decodificar(self,codificacion, entera, decimal, num_variables):
        #print("Codificacion: {}".format(codificacion))
        valores=[]
        longitud_variable = entera + decimal+1
        v_inicio=0
        v_fin=longitud_variable
        for v in range(0,num_variables):
            variable = codificacion[v_inicio:v_fin]
            valores.append(variable)
            v_inicio=v_fin
            v_fin=v_inicio+longitud_variable
            #print("Variable: {}".format(variable))
        #print(valores)
        valores_reales=[]
        for l in valores:
            #print(l)
            num=BinDecoder(l,decimal)
            #print(num.binToValor())
            valores_reales.append(num.getValor())
        #print(valores_reales)        
        return valores_reales

    def evaluar(self,funcion,poblacion,entero,decimal,nvars):
        p_evaluada = []
        for p in poblacion:
            decof=self.decodificar(p,entero,decimal,nvars)
            valor=funcion.evaluar(decof)
            p_evaluada.append([p,valor])
        return p_evaluada
        
    def ordenarPoblacion(self,poblacion):
        p_evaluada=sorted(poblacion,key=itemgetter(1))
        return p_evaluada

    def duplicarPoblacion(self,poblacion):
        double_poblacion=poblacion+poblacion
        return double_poblacion

    def duplicarPopSimple(self,poblacion):
        poblacion_simple=[]
        x=[row[0] for row in poblacion]
        poblacion_simple=x+x
        return poblacion_simple

    def matarPoblacion(self,poblacion):
        #print("matar poblacion:{}".format(poblacion))
        for i in range(self.n):
            #print("I:{}, poblacion:{}".format(i,len(poblacion)))
            poblacion.pop()
            #print("I:{}, poblacion:{}".format(i,len(poblacion)))
        return poblacion
    
    def run(self,ciclos, n, pc, pm, entero, decimal, vars, funcion, expresion=""):
        self.n = n
        self.pc = pc
        self.pm = pm
        self.lon = vars*(entero+decimal+1)
        self.b2m = self.lon*self.n*self.pm
        self.expresion=expresion
        print("b2m:{}".format(self.b2m))
        poblacion=self.inicializar()
        print("p_inicial:{},len:{}".format(poblacion,len(poblacion)))
        print("*"*60)
        p_evaluada = self.evaluar(funcion,poblacion,entero,decimal,vars)
        print("p_evaluada:{},len:{}".format(p_evaluada,len(p_evaluada)))
        print("*"*60)
        p_ordenada = self.ordenarPoblacion(p_evaluada)
        print("P_Ordenanda:{},len:{}".format(p_ordenada,len(p_ordenada)))
        print("*"*60)
        generacion=0
        while(generacion<ciclos):
            generacion += 1
            #pdoble=self.duplicarPoblacion(p_ordenada)
            pdoble=self.duplicarPopSimple(p_ordenada)
            
            for i in range(math.ceil(self.n/2)):
                rand=random.randint(0, 1)
                #rand=0.5
                if(rand<pc):
                    locus=random.randint(1, math.ceil((self.lon/2)+1))
                    #print("part:{}".format(locus))
                    A=pdoble[self.n+i]
                    B=pdoble[2*self.n-1-i]                    
                    #print("A:{}, B:{},i:{}".format(A,B,i))
                    AL=A[0:locus]
                    AM=A[locus:self.lon-locus]
                    AR=A[self.lon-locus:self.lon]
                    BL=B[0:locus]
                    BM=B[locus:self.lon-locus]
                    BR=B[self.lon-locus:self.lon]
                    #print("AL:{},AM:{},AR:{}".format(AL,AM,AR))
                    #print("BL:{},BM:{},BR:{}".format(BL,BM,BR))
                    newA=BL+AM+BR
                    newB=AL+BM+AR
                    #print("newA:{},newB:{}".format(newA,newB))
                    pdoble.pop(self.n+i)
                    pdoble.insert(self.n+i,newA)
                    pdoble.pop(2*self.n-1-i)
                    pdoble.insert(2*self.n-1-i,newB)
            print("p_pareada:{},len:{}".format(pdoble,len(pdoble)))
            print("*"*60)
            p_mutada=self.recombinar(pdoble,vars)            
            print("p_mutada:{},len:{}".format(p_mutada,len(p_mutada)))
            print("*"*60)
            p_evaluada = self.evaluar(funcion,p_mutada,entero,decimal,vars)
            print("p_evaluada:{},len:{}".format(p_evaluada,len(p_evaluada)))
            print("*"*60)
            p_ordenada = self.ordenarPoblacion(p_evaluada)
            print("p_ordenanda:{},len:{}".format(p_ordenada,len(p_ordenada)))
            print("*"*60)
            p_ordenada=self.matarPoblacion(p_ordenada)    
            print("muerte P:{},len:{}".format(p_ordenada,len(p_ordenada)))
            print("*"*60)
        return p_ordenada        

    def __genIndividuo(self,longitud):
        codificacion=''
        for i in range(longitud):
            val = random.randint(0, 1)
            codificacion+=str(val)
        return codificacion
