class BinDecoder:
    cadena=""
    presicion=0

    def __init__(self, bits, presicion):
        self.cadena = bits
        self.presicion = presicion
    
    def getValor(self):
        longitud=len(self.cadena)
        signo=self.cadena[0]
        if signo is '0':
            factor=1
        else:
            factor=-1
        entero=self.cadena[1:longitud-self.presicion]
        decimal=self.cadena[longitud-self.presicion:]
        x=self.__binToEntero(entero)
        y=self.__bintoDec(decimal)
        z=(x+y)*factor
        #print("Cadena:{}, X: {},Y: {},Factor: {}".format(self.cadena,x,y,factor))
        return z

    def __binToEntero(self,entero):
        if entero is '':
            return 0
        else:
            i=len(entero)
            sum=0
            for g in entero:
                sum+=pow(2,i-1)*int(g)
                i-=1
            return sum

    def __bintoDec(self,decimal):
        if decimal is '':
            return 0
        else:
            i=1
            sum=0
            for g in decimal:
                sum+=(1/2**(i))*int(g)
                i+=1
            return sum
