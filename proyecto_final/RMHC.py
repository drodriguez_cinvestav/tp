from Heuristica import *
from BinDecoder import *
import random

class RMHC(Heuristica):
    expresion=""
    def inicializar(self,entera,decimal,num_variables,funcion_objetivo):
        individuo = self.__genIndividuo(num_variables*(entera+decimal+1))
        #print("Individuo:{}".format(individuo))
        valores_decimales = self.decodificar(individuo,entera,decimal,num_variables)
        best=self.evaluar(funcion_objetivo,valores_decimales)
        return [individuo,best]

    def evaluar(self,funcion_objetivo,valores):
        #print("*"*200)
        #print(self.expresion)
        if(self.expresion!=""):
            print("#"*200)
            z=funcion_objetivo.evaluar(valores, self.expresion)
        else:
            z=funcion_objetivo.evaluar(valores)
            print("*"*200)
        return z

    def recombinar(self,codificacion):
        len_codificacion=len(codificacion)
        pos_random=random.randint(0, len_codificacion-1)
        valor=codificacion[pos_random]
        if valor==0:
            codificacion[pos_random] = 1
        else:
            codificacion[pos_random] = 0
        return codificacion

    def decodificar(self, codificacion, entera, decimal, num_variables):
        #print("Codificacion: {}".format(codificacion))
        valores=[]
        longitud_variable = entera + decimal+1
        v_inicio=0
        v_fin=longitud_variable
        for v in range(0,num_variables):
            variable = codificacion[v_inicio:v_fin]
            valores.append(variable)
            print("v_inicio:{},v_fin:{}".format(v_inicio,v_fin))
            v_inicio=v_fin
            v_fin=v_inicio+longitud_variable
            print("Variable: {}".format(variable))
        print(valores)
        valores_reales=[]
        for l in valores:
            #print(l)
            num=BinDecoder(l,decimal)
            print(num.getValor())
            valores_reales.append(num.getValor())
        #print(valores_reales)        
        return valores_reales

    def run(self,ciclos,entero,decimal,num_variables,funcion_objetivo,expresion=""):
        self.expresion=expresion
        x=self.inicializar(entero,decimal,num_variables,funcion_objetivo)
        individuo=x[0]
        best_fitness = x[1]
        best_i=individuo[:]
        b_valores_reales=[]
        print("Iteracion:{}, i:{}, best0:{}".format(0,individuo, best_fitness))
        generacion = 0
        while(generacion < ciclos):
            generacion += 1
            individuo=self.recombinar(individuo)
            valores_reales=self.decodificar(individuo,entero,decimal,num_variables)
            print("Gen: {}, valores: {}".format(generacion,valores_reales))
            z=self.evaluar(funcion_objetivo,valores_reales)
            if(z < best_fitness):
                best_fitness=z
                best_i=individuo[:]
                b_valores_reales=valores_reales[:]
                print("Iteracion:{}, i:{}, best:{}".format(generacion, best_i,best_fitness))
            print("Gen:{}, i:{}, best:{}".format(generacion, individuo,best_fitness))
        return [ best_fitness,best_i,b_valores_reales]

    def __genIndividuo(self, longitud):
        codificacion = []
        for i in range(longitud):
            val = random.randint(0, 1)
            codificacion.append(val)
        return codificacion
