from HeuristicaFactory import *
from FuncionesFactory import *

if __name__ == "__main__":
    heuristica="EGA"
    funcion="dejongs"
    #funcion="axis_parallel"
    
    hFactory = HeuristicaFactory()
    fFactory = FuncionesFactory()

    funcion_objetivo=fFactory.getFuncion(funcion)
    h = hFactory.getHeuristica(heuristica)

    if heuristica == "RMHC":
        #h.run(Iteraciones,Entero,Decimal,num_variables,funcion)
        vars=4
        e=h.run(1000,8,4,vars,funcion_objetivo)
        print("#"*50)
        print("Función: {}".format(funcion))
        print("Núm Variables:{}".format(vars))
        print("Best:{}".format(e[0]))
        print("Individuo:{}".format(e[1]))
        print("Valores: {}".format(e[2]))
        print("#"*50)
    elif heuristica == "EGA":
        #h.run(Ciclos,Individuos,P_Cruza,P_mutacion,Entero,Decimal,num_variables,funcion)
        vars=2
        h.run(1,5, 0.8, 0.1, 8, 4, vars, funcion_objetivo)
    else:
        print("Heuristica no encontrada")