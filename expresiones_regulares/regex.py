import re

regex = input('Introdusca la expresion regular: ')

loop=True

while loop:
    print(regex)
    txt = input('Cadena a validar: ')
    x = re.search(regex, txt)
    if (x):
        print("YES! We have a match!")
        print(x.group())
    else:
        print("No match")
    val=input('Desea continuar? Y/N... ')
    if val.upper() == "N":
        loop=False
